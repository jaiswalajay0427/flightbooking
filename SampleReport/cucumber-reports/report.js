$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/Login.feature");
formatter.feature({
  "name": "Checkout and Purches",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@payment"
    }
  ]
});
formatter.scenario({
  "name": "Select Product and Pay with valid Card Details",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@payment"
    },
    {
      "name": "@happyflow"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "user logged into Application",
  "keyword": "Given "
});
formatter.match({
  "location": "runner.Steps.user_logged_into_Application()"
});
formatter.embedding("image/png", "embedded0.png", "HomePage");
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user select Source \"Paris\" and Destination \"London\"",
  "keyword": "When "
});
formatter.match({
  "location": "runner.Steps.user_select_Source_and_Destination(java.lang.String,java.lang.String)"
});
formatter.embedding("image/png", "embedded1.png", "Itenary Planner");
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User select flight",
  "keyword": "And "
});
formatter.match({
  "location": "runner.Steps.user_select_flight()"
});
formatter.embedding("image/png", "embedded2.png", "HomePage");
formatter.embedding("image/png", "embedded3.png", "SelectFLight");
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User make payment Cardnum  \"4111111111111111\" Name \"TestUser\"",
  "keyword": "And "
});
formatter.match({
  "location": "runner.Steps.user_make_payment_Cardnum_Name(java.lang.String,java.lang.String)"
});
formatter.embedding("image/png", "embedded4.png", "Purchase Page");
formatter.embedding("image/png", "embedded5.png", "Payment Details");
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verify and record confirmation id",
  "keyword": "Then "
});
formatter.match({
  "location": "runner.Steps.verify_and_record_confirmation_id()"
});
formatter.embedding("image/png", "embedded6.png", "BlazeDemo Confirmation");
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Select Product and Pay with valid Card Details",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@payment"
    },
    {
      "name": "@happyflow"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "user logged into Application",
  "keyword": "Given "
});
formatter.match({
  "location": "runner.Steps.user_logged_into_Application()"
});
formatter.embedding("image/png", "embedded7.png", "HomePage");
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user select Source \"Boston\" and Destination \"Dublin\"",
  "keyword": "When "
});
formatter.match({
  "location": "runner.Steps.user_select_Source_and_Destination(java.lang.String,java.lang.String)"
});
formatter.embedding("image/png", "embedded8.png", "Itenary Planner");
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User select flight",
  "keyword": "And "
});
formatter.match({
  "location": "runner.Steps.user_select_flight()"
});
formatter.embedding("image/png", "embedded9.png", "HomePage");
formatter.embedding("image/png", "embedded10.png", "SelectFLight");
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User make payment Cardnum  \"4111111111111111\" Name \"TestUser\"",
  "keyword": "And "
});
formatter.match({
  "location": "runner.Steps.user_make_payment_Cardnum_Name(java.lang.String,java.lang.String)"
});
formatter.embedding("image/png", "embedded11.png", "Purchase Page");
formatter.embedding("image/png", "embedded12.png", "Payment Details");
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verify payment Details",
  "keyword": "Then "
});
formatter.match({
  "location": "runner.Steps.verify_payment_Details()"
});
formatter.embedding("image/png", "embedded13.png", "COnfirmation Details");
formatter.result({
  "error_message": "java.lang.AssertionError: Validation : is Payment Amount Match Confirmation Amt: 555 Payment Amt: 914.76\r\n\tat org.junit.Assert.fail(Assert.java:89)\r\n\tat org.junit.Assert.assertTrue(Assert.java:42)\r\n\tat pages.Confirmationpage.validate(Confirmationpage.java:74)\r\n\tat pages.Confirmationpage.paymentAmount(Confirmationpage.java:66)\r\n\tat runner.Steps.verify_payment_Details(Steps.java:79)\r\n\tat ✽.verify payment Details(file:///C:/Project/Java/Workspace/flight/flightbooking/src/test/resources/features/Login.feature:18)\r\n",
  "status": "failed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Select Product and Pay with valid Card Details and validate Ammount",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@payment"
    },
    {
      "name": "@unhappyflow"
    },
    {
      "name": "@validateAmt"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "user logged into Application",
  "keyword": "Given "
});
formatter.match({
  "location": "runner.Steps.user_logged_into_Application()"
});
formatter.embedding("image/png", "embedded14.png", "HomePage");
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user select Source \"Paris\" and Destination \"London\"",
  "keyword": "When "
});
formatter.match({
  "location": "runner.Steps.user_select_Source_and_Destination(java.lang.String,java.lang.String)"
});
formatter.embedding("image/png", "embedded15.png", "Itenary Planner");
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User select flight",
  "keyword": "And "
});
formatter.match({
  "location": "runner.Steps.user_select_flight()"
});
formatter.embedding("image/png", "embedded16.png", "HomePage");
formatter.embedding("image/png", "embedded17.png", "SelectFLight");
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User make payment Cardnum  \"4111111111111111\" Name \"TestUser\"",
  "keyword": "And "
});
formatter.match({
  "location": "runner.Steps.user_make_payment_Cardnum_Name(java.lang.String,java.lang.String)"
});
formatter.embedding("image/png", "embedded18.png", "Purchase Page");
formatter.embedding("image/png", "embedded19.png", "Payment Details");
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verify payment Details",
  "keyword": "Then "
});
formatter.match({
  "location": "runner.Steps.verify_payment_Details()"
});
formatter.embedding("image/png", "embedded20.png", "COnfirmation Details");
formatter.result({
  "error_message": "java.lang.AssertionError: Validation : is Payment Amount Match Confirmation Amt: 555 Payment Amt: 914.76\r\n\tat org.junit.Assert.fail(Assert.java:89)\r\n\tat org.junit.Assert.assertTrue(Assert.java:42)\r\n\tat pages.Confirmationpage.validate(Confirmationpage.java:74)\r\n\tat pages.Confirmationpage.paymentAmount(Confirmationpage.java:66)\r\n\tat runner.Steps.verify_payment_Details(Steps.java:79)\r\n\tat ✽.verify payment Details(file:///C:/Project/Java/Workspace/flight/flightbooking/src/test/resources/features/Login.feature:26)\r\n",
  "status": "failed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Select Product and Pay with invalid/expired Card Details",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@payment"
    },
    {
      "name": "@unhappyflow"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "user logged into Application",
  "keyword": "Given "
});
formatter.match({
  "location": "runner.Steps.user_logged_into_Application()"
});
formatter.embedding("image/png", "embedded21.png", "HomePage");
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user select Source \"Paris\" and Destination \"London\"",
  "keyword": "When "
});
formatter.match({
  "location": "runner.Steps.user_select_Source_and_Destination(java.lang.String,java.lang.String)"
});
formatter.embedding("image/png", "embedded22.png", "Itenary Planner");
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User select flight",
  "keyword": "And "
});
formatter.match({
  "location": "runner.Steps.user_select_flight()"
});
formatter.embedding("image/png", "embedded23.png", "HomePage");
formatter.embedding("image/png", "embedded24.png", "SelectFLight");
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User make payment Cardnum  \"0111111111111111\" Name \"TestUser\"",
  "keyword": "And "
});
formatter.match({
  "location": "runner.Steps.user_make_payment_Cardnum_Name(java.lang.String,java.lang.String)"
});
formatter.embedding("image/png", "embedded25.png", "Purchase Page");
formatter.embedding("image/png", "embedded26.png", "Payment Details");
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "same Source and destination",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@payment"
    },
    {
      "name": "@unhappyflow"
    },
    {
      "name": "@wrongDestination"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "user logged into Application",
  "keyword": "Given "
});
formatter.match({
  "location": "runner.Steps.user_logged_into_Application()"
});
formatter.embedding("image/png", "embedded27.png", "HomePage");
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user select Source \"Paris\" and Destination \"Paris\"",
  "keyword": "When "
});
formatter.match({
  "location": "runner.Steps.user_select_Source_and_Destination(java.lang.String,java.lang.String)"
});
formatter.embedding("image/png", "embedded28.png", "SelectFLight");
formatter.result({
  "error_message": "junit.framework.AssertionFailedError: Paris is not a valid port, please provide Valid Port,\r\n\tat junit.framework.Assert.fail(Assert.java:57)\r\n\tat junit.framework.Assert.assertTrue(Assert.java:22)\r\n\tat pages.Homepage.selectdropdown(Homepage.java:98)\r\n\tat pages.Homepage.selectPorts(Homepage.java:52)\r\n\tat runner.Steps.user_select_Source_and_Destination(Steps.java:47)\r\n\tat ✽.user select Source \"Paris\" and Destination \"Paris\"(file:///C:/Project/Java/Workspace/flight/flightbooking/src/test/resources/features/Login.feature:42)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "User select flight",
  "keyword": "And "
});
formatter.match({
  "location": "runner.Steps.user_select_flight()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User make payment Cardnum  \"4111111111111111\" Name \"TestUser\"",
  "keyword": "And "
});
formatter.match({
  "location": "runner.Steps.user_make_payment_Cardnum_Name(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify payment Details",
  "keyword": "Then "
});
formatter.match({
  "location": "runner.Steps.verify_payment_Details()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
});