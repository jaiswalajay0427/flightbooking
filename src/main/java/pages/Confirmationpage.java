package pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import utility.BasePage;

public class Confirmationpage extends BasePage {
	
	
	
	@FindBy(xpath = "//td[text()='Id']/following-sibling::td")
	protected WebElement confirmationID;
	
	@FindBy(xpath = "//td[text()='Amount']/following-sibling::td")
	protected WebElement actualAmount;
	
	
	
	public void confirmation() {
		
		takescreenshot("BlazeDemo Confirmation");
		if(driver.getTitle().contains("BlazeDemo Confirmation"))
		{
			
			Assert.assertTrue("BlazeDemo Confirmation", true);
		}
		else
		{
			Assert.assertTrue("Application Failes to load Home Page", false);	
		}
		
	}
	


	public Confirmationpage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}


	public void getconfermationId() {
		// TODO Auto-generated method stub
		String COnfirmationID= confirmationID.getText();
		System.out.println(COnfirmationID);
		
	}



	public void cardDetails() {
		// TODO Auto-generated method stub
		
	}



	public void paymentAmount() {

		String ConfAmmount = actualAmount.getText();
		ConfAmmount = ConfAmmount.substring(0,ConfAmmount.length()-4);
		takescreenshot("COnfirmation Details");
		validate("Validation : is Payment Amount Match ", ConfAmmount, Ammount);
		
	}



	private void validate(String message, String confAmmount, String ammount) {
		// TODO Auto-generated method stub
		Assert.assertTrue(message + "Confirmation Amt: "+ confAmmount +" Payment Amt: "+ammount , confAmmount.contentEquals(ammount));
	}



	

}
