package pages;

import java.util.List;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.util.concurrent.Service.State;

import utility.BasePage;

public class PaymentPage extends BasePage {
	WebDriverWait wait = new WebDriverWait(driver, 30);

	@FindBy(id = "inputName")
	private WebElement name;

	@FindBy(id = "address")
	private WebElement address;

	@FindBy(id = "city")
	private WebElement city;

	@FindBy(id = "state")
	private WebElement state;

	@FindBy(id = "zipCode")
	private WebElement zipCode;

	@FindBy(xpath = "cardType")
	private WebElement cardType;

	@FindBy(id = "creditCardNumber")
	private WebElement creditCardNumber;

	@FindBy(id = "creditCardMonth")
	private WebElement creditCardMonth;

	@FindBy(id = "creditCardYear")
	private WebElement creditCardYear;

	@FindBy(id = "nameOnCard")
	private WebElement nameOnCard;
	
	@FindBy(xpath = "//p[text()='Total Cost: ']/em")
	private WebElement ammount;
	
	

	public void purchase() {
		
		
		if(driver.getTitle().contains("BlazeDemo Purchase"))
		{
			takescreenshot("Purchase Page");
			Assert.assertTrue("you are in Home Page", true);
		}
		else
		{
			Assert.assertTrue("Application Failes to load Home Page", false);	
		}
		
	}

	public void provideDetails(String CardNum, String Name) {

		
		Ammount = ammount.getText();
		cardNumb =  cardNumb;
		name.sendKeys("Test Payment");
		address.sendKeys("Nuaimiya st");
		city.sendKeys("Dubai");
		state.sendKeys("UAE");
		zipCode.sendKeys("3456");
		//cardtype
		creditCardNumber.sendKeys(CardNum);
		creditCardMonth.sendKeys("11");
		creditCardYear.sendKeys("24");
		nameOnCard.sendKeys(Name);
		takescreenshot("Payment Details");
		submitbtn.click();
		
		
		


	}

	

	protected void click(WebElement elm) {

		wait.until(ExpectedConditions.elementToBeClickable(elm));
		elm.click();
	}

	protected void waitForElem(WebElement elm) {
		/*
		 * if(wait.until(ExpectedConditions.elementToBeClickable(elm)) == null)
		 * return false; return true;
		 */
		wait.until(ExpectedConditions.elementToBeClickable(elm));

	}

	public PaymentPage(WebDriver driver) {
		super(driver);
	}

}
