package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.core.gherkin.Scenario;
import junit.framework.Assert;
import utility.BasePage;

public class Homepage extends BasePage {
	WebDriverWait wait = new WebDriverWait(driver, 30);
	@FindBy(xpath = "//h1[text()='Welcome to the Simple Travel Agency!']")
	private WebElement Homepageheader;

	@FindBy(xpath = "//select[@name='fromPort']")
	private WebElement fromPort;

	@FindBy(xpath = "//select[@name='toPort']")
	private WebElement toPort;

	public void isloginPage() {

		waitForElem(Homepageheader);

		/*
		 * if (Homepageheader.isDisplayed()) takescreenshot("HomePage"); else
		 * Assert.assertFalse(true);
		 */

		if (driver.getTitle().contains("BlazeDemo")) {
			takescreenshot("HomePage");
			Assert.assertTrue("you are in Home Page", true);
		} else {
			Assert.assertTrue("Application Failes to load Home Page", false);
		}

	}

	public void selectPorts(String source, String destination) {


			selectdropdown(fromPort, source);
			selectdropdown(toPort, destination);
			takescreenshot("Itenary Planner");
			submitbtn.click();
			

	}

	public void selectflight() {

		if (driver.getTitle().contains("BlazeDemo - reserve")) {
			takescreenshot("HomePage");
			Assert.assertTrue("you are in Home Page", true);
		} else {
			Assert.assertTrue("Application Failes to load Home Page", false);
		}
		takescreenshot("SelectFLight");
		submitbtn.click();
		
	}

	protected void click(WebElement elm) {

		wait.until(ExpectedConditions.elementToBeClickable(elm));
		elm.click();
	}

	protected void waitForElem(WebElement elm) {
		/*
		 * if(wait.until(ExpectedConditions.elementToBeClickable(elm)) == null)
		 * return false; return true;
		 */
		wait.until(ExpectedConditions.elementToBeClickable(elm));

	}

	public Homepage(WebDriver driver) {
		super(driver);
	}

	private void selectdropdown(WebElement elem, String value) {
		try {

			Select sel = new Select(elem);
			sel.selectByValue(value);
		} catch (org.openqa.selenium.NoSuchElementException nse) {
			takescreenshot("SelectFLight");
			Assert.assertTrue(value +" is not a valid port, please provide Valid Port,", false);
		}
		

	}

}
