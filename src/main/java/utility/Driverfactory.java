package utility;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Driverfactory {

	public static WebDriver driver = null;

	/*
	 * public static SingletonDesignDemo2 getObject() { if(obj==null) { obj= new
	 * SingletonDesignDemo2(); } return obj; }
	 */

	public static WebDriver Driver() {
		// Use Of Singleton Concept and Initialize webDriver
		if (driver == null) {
			String sufix = "";
			String path = System.getProperty("user.dir") + "\\Exe";
			String OS = System.getProperty("os.name");
			//System.out.println(OS); // Windows 10
			if(OS.contains("Windows"))
				sufix = ".exe";

			String browser = ConstantVariable.browserName;
			switch (browser) {
			case "chrome":
				System.setProperty("webdriver.chrome.driver", path + "\\chromedriver"+sufix);
				driver = new ChromeDriver();
				break;
			case "Firefox":
				System.setProperty("webdriver.gecko.driver", path + "\\geckodriver"+sufix);
				driver = new FirefoxDriver();
				break;
			case "IE":
				System.setProperty("webdriver.edge.driver", path + "\\MicrosoftWebDriver"+sufix);
				driver = new EdgeDriver();
				break;
			default:
				throw new IllegalArgumentException("Browser Not suported" + browser);
			}

			/*
			 * if(ConstantVariable.browserName.equalsIgnoreCase("chrome")) {
			 * System.setProperty("webdriver.chrome.driver", path
			 * +"\\chromedriver.exe"); driver=new ChromeDriver(); } else
			 * if(ConstantVariable.browserName.equalsIgnoreCase("Firefox")) {
			 * System.setProperty("webdriver.gecko.driver", path
			 * +"\\geckodriver.exe"); driver=new FirefoxDriver(); } else
			 * if(ConstantVariable.browserName.equalsIgnoreCase("IE")) {
			 * System.setProperty("webdriver.edge.driver", path
			 * +"\\MicrosoftWebDriver.exe"); driver=new EdgeDriver(); }
			 */
		}

		// Perform Basic Operations
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.get(ConstantVariable.URl);
		return driver;
	}

	private static void swicth() {
		// TODO Auto-generated method stub

	}

	public static void quit() {
		driver.quit();
		driver = null; // we destroy the driver object after quit operation
	}

	public static void close() {
		driver.close();
		driver = null; // we destroy the driver object after quit operation
	}

	public static void openurl(String URL) {
		driver.get(URL);
	}

	/*
	 * public static void main(String[] arg) { Driver(); BasePage bp = new
	 * BasePage(driver); bp.selectProdandCheckout();
	 * 
	 * 
	 * }
	 */

}
