@payment
Feature: Checkout and Purches 
# no broken links 
@happyflow
  Scenario: Select Product and Pay with valid Card Details
   Given user logged into Application
    When user select Source "Paris" and Destination "London"
    And User select flight
    And User make payment Cardnum  "4111111111111111" Name "TestUser"
    Then verify and record confirmation id

 @happyflow
  Scenario: Select Product and Pay with valid Card Details
   Given user logged into Application
    When user select Source "Boston" and Destination "Dublin"
    And User select flight
    And User make payment Cardnum  "4111111111111111" Name "TestUser"
    Then verify payment Details
    
 @unhappyflow @validateAmt
  Scenario: Select Product and Pay with valid Card Details and validate Ammount
   Given user logged into Application
    When user select Source "Paris" and Destination "London"
    And User select flight
    And User make payment Cardnum  "4111111111111111" Name "TestUser"
    Then verify payment Details
   
   
 @unhappyflow 
  Scenario: Select Product and Pay with invalid/expired Card Details
   Given user logged into Application
    When user select Source "Paris" and Destination "London"
    And User select flight
    And User make payment Cardnum  "0111111111111111" Name "TestUser"
    #Then verify payment Details
     
     
     
 @unhappyflow @wrongDestination
  Scenario: same Source and destination
   Given user logged into Application
    When user select Source "Paris" and Destination "Paris"
    And User select flight
    And User make payment Cardnum  "4111111111111111" Name "TestUser"
    Then verify payment Details
    # for current ex source and Destination are completly diffrent cities, but in real project the may overlap