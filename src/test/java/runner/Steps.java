package runner;

import org.junit.Assert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.Confirmationpage;
import pages.Homepage;
import pages.PaymentPage;
import utility.BasePage;
import utility.Driverfactory;

public class Steps {

	WebDriver driver;
	BasePage base = null;
	static Homepage home = null;
	PaymentPage pay = null;
	static Confirmationpage conf = null;
	// Scenario scenario;

	@Before
	public void before(Scenario scenario) {

		driver = Driverfactory.Driver();
		BasePage.scenario = scenario;
		// System.out.println(scenario.getId());
	}

	@Given("user logged into Application")
	public void user_logged_into_Application() {
		home = new Homepage(driver);
		home.isloginPage();

	}

	@When("user select Source {string} and Destination {string}")
	public void user_select_Source_and_Destination(String Source, String Destination) {
		//home = new Homepage(driver);
		home.selectPorts(Source, Destination);

	}

	@When("User select flight")
	public void user_select_flight() {
		home.selectflight();

	}

@When("User make payment Cardnum  {string} Name {string}")
public void user_make_payment_Cardnum_Name(String CardNum, String cvv) {
		pay = new PaymentPage(driver);
		pay.purchase();
		pay.provideDetails(CardNum, cvv);

	}

	@Then("verify and record confirmation id")
	public void verify_and_record_confirmation_id() {
		conf= new Confirmationpage(driver);
		conf.confirmation();
		conf.getconfermationId();

	}


@Then("verify payment Details")
public void verify_payment_Details() {
    // Write code here that turns the phrase above into concrete actions
	conf= new Confirmationpage(driver);
	//conf.cardDetails();
	conf.paymentAmount();
}

	@After
	public void after(Scenario scenario) {
		base = new BasePage(driver);
		// this.scenario = scenario;
		// System.out.println(scenario.getId());
		System.out.println(scenario.getStatus());
		/*
		 * base.driver.close(); base.driver.quit(); base.driver= null; driver =
		 * null;
		 */
		// scenario.attach(((TakesScreenshot)
		// driver).getScreenshotAs(OutputType.BYTES), "image/png", "ds");
	}
}
